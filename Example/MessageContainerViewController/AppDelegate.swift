/*
 Copyright (c) 2016 Jan Bartel <barteljan@yahoo.de>
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 */

import UIKit
import MessageContainerViewController

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        self.window = UIWindow()//(frame: UIScreen.main.bounds)
        
        let cfg = MessageColorConfiguration(infoColor: .green, errorColor: .red, warningColor: .yellow, infoTextColor: .white, errorTextColor: .white, warningTextColor: .white)
        
        let viewController = TestViewController(nibName: "TestViewController", bundle: nil)
        viewController.title = "TestController"
        let tabbarController = UITabBarController(nibName: nil, bundle: nil)
        let navigationController = UINavigationController(rootViewController: viewController)
        let outerNavController = UINavigationController(rootViewController: tabbarController)
        tabbarController.viewControllers = [navigationController]
        let messageController = TopMessageContainerViewController(rootViewController: outerNavController, colorConfiguration: cfg)
        outerNavController.isToolbarHidden = true
        messageController.font = UIFont.systemFont(ofSize: 20)
        viewController.messageController = messageController
        self.window?.rootViewController = messageController
        
        self.window?.makeKeyAndVisible()
        return true
    }

}

