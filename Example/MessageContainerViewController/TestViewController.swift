/*
 Copyright (c) 2016 Jan Bartel <barteljan@yahoo.de>
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 */

import UIKit
import MessageContainerViewController

class TestViewController: UIViewController {

    var messageController : MessageViewControllerProtocol!
    //var outerController: MessageViewControllerProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func showError(_ sender: AnyObject) {
        self.messageController.showErrorMessage("This is an error, with more than one line, believe it or not ;)", animated: true, durationToClose: 2)
    }
    
    @IBAction func toggleStayingMessage(_ sender: Any) {
        if self.messageController.isShowingStayingMessage{
            self.messageController.hideStayingMessage(animated: true)
        }else{
            self.messageController.showStayingInfoMessage("I'm here to stay. And I have at least one linebreak.", animated: true)
        }
    }
    
    @IBAction func setMessage(_ sender: Any) {
        self.messageController.showStayingInfoMessage("You just set this message", animated: true)
    }
    
    @IBAction func toggleConstrainToSafeArea(_ sender: Any) {
        let newVal = !self.messageController.constrainToSafeArea
        self.messageController.constrainToSafeArea =  newVal
    }
    
    
    @IBAction func showmessage(_ sender: Any) {
        self.messageController.showInfoMessage("Hello World!", animated: true, durationToClose: 2.0)
    }
    
    /*
    @IBAction func showOuterError(_ sender: Any) {
        self.outerController?.showErrorMessage("Outer View: This is an error, with more than one line, believe it or not ;)", animated: true, durationToClose: 2)
    }
 */
    
}
