/*
 Copyright (c) 2016 Jan Bartel <barteljan@yahoo.de>
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 */


import Foundation
import UIKit

open class StayingMessageContainer: UIView {
    
}

open class StayingMessageLabel: UILabel {
    
}

open class TopMessageContainerView : UIView{
    
}

open class MessageLabelContainer: UIView {
    
}

open class MessageViewLabel: UILabel {
    
}

open class TopMessageContainerViewController: UIViewController, MessageViewControllerProtocol {
    
    //MARK: Public Attributes
    open var colorConfiguration: MessageColorConfiguration = MessageColorConfiguration.init(
        infoColor: .blue,
        errorColor: .red,
        warningColor: .yellow,
        infoTextColor: .lightText,
        errorTextColor: .darkText,
        warningTextColor: .lightText)
    
    open var verticalPadding: CGFloat = 5.0
    open var horizontalPadding: CGFloat = 5.0
    
    
    @IBOutlet open var rootViewController: UIViewController!
    //generally only useful if just sides are constrained to safearea
    //navcontrollers and tabbacontrollers have nice defaultvbehavior and get confused
    //anyway when constrained inside safe area
    open var constrainToSafeArea: Bool = true {
        didSet {
            self.updateForSafeLayoutGuide()
        }
    }
    
    open var messageVisible: Bool = false
    
    open var isShowingStayingMessage: Bool = false
    
    @objc open dynamic var font: UIFont = UIFont.systemFont(ofSize: 17)
    
    //MARK: Initializers
    public init(rootViewController: UIViewController, colorConfiguration: MessageColorConfiguration? = nil) {
        self.rootViewController = rootViewController
        
        if let colorConfiguration = colorConfiguration {
            self.colorConfiguration = colorConfiguration
        }
        
        super.init(nibName: nil, bundle: nil)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        assert(rootViewController != nil, "should have a rootViewController")
    }
    
    //MARK: controller livecycle
    open override func loadView() {
        
        let view = TopMessageContainerView(frame: .null)
        self.view = view
        view.backgroundColor = UIColor.clear
        
        self.addChild(self.rootViewController)
        self.rootViewController.view.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(self.rootViewController.view)
        
        view.addSubview(self.messageLabelContainer)
        self.messageLabelContainer.addSubview(self.messageLabel)
        
        view.addSubview(self.stayingMessageLabelContainer)
        self.stayingMessageLabelContainer.addSubview(self.stayingMessageLabel)
        
        self.bootstrapConstraints()
        
        self.rootViewController.didMove(toParent: self)
        
    }
    
    func bootstrapConstraints(){
        
        
        //staying message container
        let stayingContainerHeight = self.stayingMessageLabelContainer.heightAnchor.constraint(equalToConstant: 0.0)
        self.stayingContainerHeight = stayingContainerHeight
        
        self.topConstraintWithoutSafearea = self.stayingMessageLabelContainer.topAnchor.constraint(equalTo: self.view.topAnchor)
        NSLayoutConstraint.deactivate([self.topConstraintWithoutSafearea])
        if #available(iOS 11.0, *) {
            self.topConstraintWithSafearea = self.stayingMessageLabelContainer.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor)
        } else {
            // Fallback on earlier versions
            self.topConstraintWithSafearea = self.topConstraintWithoutSafearea
        }
        
        
        
        
        
        if #available(iOS 11.0, *) {
            self.stayingMessageTopBottomConstraints = [
                self.stayingMessageLabel.topAnchor.constraint(equalTo: self.stayingMessageLabelContainer.safeAreaLayoutGuide.topAnchor),//,constant: -self.verticalPadding),
                self.stayingMessageLabel.bottomAnchor.constraint(equalTo: self.stayingMessageLabelContainer.bottomAnchor,constant: -self.verticalPadding)
            ]
        } else {
            // Fallback on earlier versions
            self.stayingMessageTopBottomConstraints = [
                self.stayingMessageLabel.topAnchor.constraint(equalTo: self.stayingMessageLabelContainer.topAnchor),//,constant: -self.verticalPadding),
                self.stayingMessageLabel.bottomAnchor.constraint(equalTo: self.stayingMessageLabelContainer.bottomAnchor,constant: -self.verticalPadding)
            ]
        }
        
        
        NSLayoutConstraint.activate([
            stayingContainerHeight,
            self.stayingMessageLabel.centerXAnchor.constraint(equalTo: self.stayingMessageLabelContainer.centerXAnchor),
            self.stayingMessageLabel.leadingAnchor.constraint(greaterThanOrEqualTo: self.stayingMessageLabelContainer.leadingAnchor,constant: self.horizontalPadding),
            self.stayingMessageLabel.trailingAnchor.constraint(greaterThanOrEqualTo: self.stayingMessageLabelContainer.trailingAnchor,constant: -self.horizontalPadding)
        ])
        
        NSLayoutConstraint.activate(self.stayingMessageTopBottomConstraints)
        
        
        //message label container
        let messageLabelContainerHeight = self.messageLabelContainer.heightAnchor.constraint(equalToConstant: 0.0)
        self.messageLabelContainerHeight = messageLabelContainerHeight
        
        
        self.sidesNonSafeAreaConstraints = [
            self.messageLabelContainer.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            self.messageLabelContainer.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            self.stayingMessageLabelContainer.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            self.stayingMessageLabelContainer.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
        ]
        
        
        
        if #available(iOS 11.0, *) {
            self.sidesSafeAreaConstraints = [
                self.messageLabelContainer.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor),
                self.messageLabelContainer.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor),
                self.stayingMessageLabelContainer.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor),
                self.stayingMessageLabelContainer.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor),
            ]
        }else{
            sidesSafeAreaConstraints = sidesNonSafeAreaConstraints
        }
        
        
        NSLayoutConstraint.activate([
            messageLabelContainerHeight,
            
            self.messageLabelContainer.topAnchor.constraint(equalTo: self.stayingMessageLabelContainer.bottomAnchor),
            
            self.messageLabel.centerXAnchor.constraint(equalTo: self.messageLabelContainer.centerXAnchor),
            //self.messageLabel.centerYAnchor.constraint(equalTo: self.messageLabelContainer.centerYAnchor),
            
            self.messageLabel.leadingAnchor.constraint(greaterThanOrEqualTo: self.messageLabelContainer.leadingAnchor,constant: self.horizontalPadding),
            self.messageLabel.trailingAnchor.constraint(greaterThanOrEqualTo: self.messageLabelContainer.trailingAnchor,constant: -self.horizontalPadding),
        ])
        
        if #available(iOS 11.0, *) {
            self.messageLabelTopBottomConstraints = [
                self.messageLabel.topAnchor.constraint(equalTo: self.messageLabelContainer.safeAreaLayoutGuide.topAnchor, constant: self.verticalPadding),
                self.messageLabel.bottomAnchor.constraint(equalTo: self.messageLabelContainer.bottomAnchor, constant: -self.verticalPadding)
            ]
        } else {
            // Fallback on earlier versions
            self.messageLabelTopBottomConstraints = [
                self.messageLabel.topAnchor.constraint(equalTo: self.messageLabelContainer.topAnchor, constant: self.verticalPadding),
                self.messageLabel.bottomAnchor.constraint(equalTo: self.messageLabelContainer.bottomAnchor, constant: -self.verticalPadding)
            ]
        }
        
        //root view controller
        self.bottomConstraintWithoutSafeArea = self.rootViewController.view.bottomAnchor.constraint(equalTo: self.view.bottomAnchor)
        
        if #available(iOS 11.0, *) {
            self.bottomConstraintWithSafeArea = self.rootViewController.view.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor)
        } else {
            // Fallback on earlier versions
            self.bottomConstraintWithSafeArea = self.bottomConstraintWithoutSafeArea
        }
        self.leadingConstraintWithoutSafeArea =              self.rootViewController.view.leadingAnchor.constraint(equalTo: self.view.leadingAnchor)
        self.trailingConstraintWithoutSafeArea = self.rootViewController.view.trailingAnchor.constraint(equalTo: self.view.trailingAnchor)
        
        
        if #available(iOS 11.0, *) {
            self.leadingConstraintWithSafeArea = self.rootViewController.view.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor)
        } else {
            self.leadingConstraintWithSafeArea = self.leadingConstraintWithoutSafeArea
        }
        if #available(iOS 11.0, *) {
            self.trailingConstraintWithSafeArea = self.rootViewController.view.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor)
        } else {
            self.trailingConstraintWithSafeArea = self.trailingConstraintWithoutSafeArea
        }
        
        
        NSLayoutConstraint.activate([
            self.rootViewController.view.topAnchor.constraint(equalTo: self.messageLabelContainer.bottomAnchor),
        ])
        
        self.updateForSafeLayoutGuide()
    }
    
    //MARK: MessageViewControllerProtocol
    open func showErrorMessage(_ message: String, animated: Bool, durationToClose: Double?) {
        self.showMessage(message, color: self.colorConfiguration.errorColor, textColor: self.colorConfiguration.errorTextColor, animated: animated, durationToClose: durationToClose)
    }
    
    open func showInfoMessage(_ message: String, animated: Bool, durationToClose: Double?) {
        self.showMessage(message, color: self.colorConfiguration.infoColor, textColor: self.colorConfiguration.infoTextColor, animated: animated, durationToClose: durationToClose)
    }
    
    open func showWarningMessage(_ message: String, animated: Bool, durationToClose: Double?) {
        self.showMessage(message, color: self.colorConfiguration.warningColor, textColor: self.colorConfiguration.warningTextColor, animated: animated, durationToClose: durationToClose)
    }
    
    public func hideMessage(_ animated: Bool) {
        let changes = {
            self.messageVisible = false
            self.messageLabel.text = ""
            self.messageLabelContainer.backgroundColor = self.view.backgroundColor
            self.messageLabelTopBottomConstraints.forEach({ $0.isActive = false })
            self.messageLabelContainerHeight.isActive = true
        }
        
        if animated {
            self.enqeueChangesForAnimation(changes)
        } else {
            changes()
        }
    }
    
    public func hideStayingMessage(animated: Bool) {
        if !isShowingStayingMessage { return }
        isShowingStayingMessage = false
        let changes = {
            self.stayingMessageLabel.text = nil
            self.stayingMessageTopBottomConstraints.forEach({ $0.isActive = false })
            self.stayingContainerHeight.isActive = true
        }
        
        if animated {
            self.enqeueChangesForAnimation(changes)
        } else {
            changes()
        }
    }
    
    public func showStayingInfoMessage(_ message: String, animated: Bool) {
        isShowingStayingMessage = true
        let changes = {
            self.stayingContainerHeight.isActive = false
            
            self.stayingMessageLabel.text = message
            self.stayingMessageLabel.textColor = self.colorConfiguration.infoTextColor
            self.stayingMessageLabel.backgroundColor = .clear
            self.stayingMessageLabelContainer.backgroundColor = self.colorConfiguration.infoColor
            self.stayingMessageTopBottomConstraints.forEach({ $0.isActive = true })
            
        }
        if animated {
            self.enqeueChangesForAnimation(changes)
        } else {
            changes()
        }
    }
    
    //MARK: child views
    
    internal var topConstraintWithSafearea: NSLayoutConstraint!
    internal var topConstraintWithoutSafearea: NSLayoutConstraint!
    internal var bottomConstraintWithSafeArea: NSLayoutConstraint!
    internal var bottomConstraintWithoutSafeArea: NSLayoutConstraint!
    internal var leadingConstraintWithoutSafeArea :NSLayoutConstraint!
    internal var trailingConstraintWithoutSafeArea: NSLayoutConstraint!
    internal var leadingConstraintWithSafeArea :NSLayoutConstraint!
    internal var trailingConstraintWithSafeArea: NSLayoutConstraint!
    internal var sidesSafeAreaConstraints: [NSLayoutConstraint] = []
    internal var sidesNonSafeAreaConstraints: [NSLayoutConstraint] = []
    
    
    internal lazy var  messageLabelContainer: MessageLabelContainer = {
        let view = MessageLabelContainer()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.clipsToBounds = true
        return view
    }()
    
    internal var messageLabelContainerHeight: NSLayoutConstraint!
    internal var messageLabelTopBottomConstraints = [NSLayoutConstraint]()
    
    open lazy var messageLabel: UILabel = {
        
        let screenWidth = UIScreen.main.bounds.width
        
        let label = MessageViewLabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        
        label.font = self.font
        label.textAlignment = .center
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.preferredMaxLayoutWidth = screenWidth
        label.backgroundColor = .clear
        
        return label
    }()
    
    
    
    
    internal var stayingContainerHeight: NSLayoutConstraint!
    internal var stayingMessageTopBottomConstraints = [NSLayoutConstraint]()
    
    internal lazy var stayingMessageLabelContainer: StayingMessageContainer = {
        let view = StayingMessageContainer()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.clipsToBounds = true
        return view
    }()
    
    open lazy var stayingMessageLabel: StayingMessageLabel = {
        let screenWidth = UIScreen.main.bounds.width
        let label: StayingMessageLabel = StayingMessageLabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = self.font
        label.textAlignment = .center
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.preferredMaxLayoutWidth = screenWidth
        label.backgroundColor = .clear
        return label
    }()
    
    
    
    //MARK: enqueueing animations
    var animationQueue: [()->()] = []
    open internal(set) var isAnimating = false
    
    var pausing = false
    func popAndGo(){
        
        guard !self.animationQueue.isEmpty else{
            self.isAnimating = false
            return
        }
        if self.pausing
        {
            return
        }
        let first = self.animationQueue.remove(at: 0)
        self.isAnimating = true
        self.view.layoutIfNeeded()
            print("Starting new Animation")
            UIView.animate(withDuration: 0.5, animations: {
                first()
                self.view.layoutIfNeeded()
            },completion: {_ in print("finished another one");self.popAndGo()})
    }
    
    func enqeueChangesForAnimation(_ changes: @escaping ()->()){
        self.animationQueue.append(changes)
        if !self.isAnimating{
            self.popAndGo()
        }
    }
    
    open func pauseAnimations(){
        self.pausing = true
    }
    open func continueAnimations(){
        self.pausing = false
        self.popAndGo()
    }
    
    func animate(_ changes: @escaping () -> ()) {
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.5, animations: {
            changes()
            self.view.layoutIfNeeded()
        })
    }
    
    //MARK: Private helper functions
    internal func showMessage(_ message: String, color: UIColor, textColor: UIColor, animated: Bool, durationToClose: Double?) {
        self.messageVisible = true
        let changes = {
            self.messageLabel.text = message
            self.messageLabel.textColor = textColor
            self.messageLabelContainer.backgroundColor = color
            self.messageLabelContainerHeight.isActive = false
            self.messageLabelTopBottomConstraints.forEach({ $0.isActive = true })
        }
        
        if animated {
            self.enqeueChangesForAnimation(changes)
        } else {
            changes()
        }
        
        if(durationToClose != nil) {
            let info = ["animated": animated]
            Timer.scheduledTimer(timeInterval: durationToClose!, target: self, selector: #selector(hideTheMessage), userInfo: info, repeats: false)
        }
    }
    
    @objc func hideTheMessage(timer: Timer) {
        guard let info = timer.userInfo as? NSDictionary else { self.hideMessage(false); return }
        guard let animated = info["animated"] as? Bool else { self.hideMessage(false); return }
        self.hideMessage(animated)
    }
    
    func updateForSafeLayoutGuide() {
        self.topConstraintWithSafearea?.isActive = false
        self.topConstraintWithoutSafearea?.isActive = true
        self.bottomConstraintWithSafeArea?.isActive = false
        self.bottomConstraintWithoutSafeArea?.isActive = true
        
        
        if self.constrainToSafeArea {
            self.leadingConstraintWithoutSafeArea?.isActive = false
            self.leadingConstraintWithSafeArea?.isActive = true
            self.trailingConstraintWithoutSafeArea?.isActive = false
            self.trailingConstraintWithSafeArea?.isActive = true
            
            self.sidesNonSafeAreaConstraints.forEach{
                $0.isActive = false
            }
            self.sidesSafeAreaConstraints.forEach{
                $0.isActive = true
            }
        } else {
            self.leadingConstraintWithoutSafeArea?.isActive = true
            self.leadingConstraintWithSafeArea?.isActive = false
            self.trailingConstraintWithoutSafeArea?.isActive = true
            self.trailingConstraintWithSafeArea?.isActive = false
            
            
            self.sidesSafeAreaConstraints.forEach{
                $0.isActive = false
            }
            self.sidesNonSafeAreaConstraints.forEach{
                $0.isActive = true
            }
            
            
        }
    }
}
