/*
 Copyright (c) 2016 Jan Bartel <barteljan@yahoo.de>
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 */


import UIKit

public struct MessageColorConfiguration{
    let infoColor: UIColor
    let errorColor: UIColor
    let warningColor: UIColor
    
    let infoTextColor: UIColor
    let errorTextColor: UIColor
    let warningTextColor: UIColor
    
    public init(infoColor: UIColor, errorColor: UIColor, warningColor: UIColor, infoTextColor: UIColor, errorTextColor: UIColor, warningTextColor: UIColor) {
        self.infoColor = infoColor
        self.errorColor = errorColor
        self.warningColor = warningColor
        self.infoTextColor = infoTextColor
        self.errorTextColor = errorTextColor
        self.warningTextColor = warningTextColor
    }
}

public protocol MessageViewControllerProtocol{
    
    var colorConfiguration: MessageColorConfiguration { get }
    
    var messageVisible: Bool {get}
    
    func showErrorMessage(_ message: String,animated: Bool,durationToClose: Double?)
    
    func showInfoMessage(_ message: String,animated: Bool,durationToClose: Double?)
        
    func showWarningMessage(_ message: String,animated: Bool,durationToClose: Double?)
    
    func hideMessage(_ animated:Bool)
    
    var isShowingStayingMessage: Bool{get}
    
    func hideStayingMessage(animated: Bool)
    
    func showStayingInfoMessage(_ message: String,animated: Bool)
    
    var constrainToSafeArea: Bool{get set}
}
