# MessageContainerViewController

[![CI Status](http://img.shields.io/travis/Jan Bartel/MessageContainerViewController.svg?style=flat)](https://travis-ci.org/Jan Bartel/MessageContainerViewController)
[![Version](https://img.shields.io/cocoapods/v/MessageContainerViewController.svg?style=flat)](http://cocoapods.org/pods/MessageContainerViewController)
[![License](https://img.shields.io/cocoapods/l/MessageContainerViewController.svg?style=flat)](http://cocoapods.org/pods/MessageContainerViewController)
[![Platform](https://img.shields.io/cocoapods/p/MessageContainerViewController.svg?style=flat)](http://cocoapods.org/pods/MessageContainerViewController)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

MessageContainerViewController is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "MessageContainerViewController"
```

## Author

Jan Bartel, barteljan@yahoo.de

## License

MessageContainerViewController is available under the MIT license. See the LICENSE file for more info.
