Pod::Spec.new do |s|
  s.name             = "MessageContainerViewController"
  s.version          = "5.1.0"
  s.summary          = "A container view controller creating error messages on the top of your app"

  s.description      = <<-DESC
A container view controller creating error messages on the top of your app.
                       DESC

  s.homepage         = "https://bitbucket.org/barteljan/messagecontainerviewcontroller.git"
  s.license          = 'MIT'
  s.author           = { "Jan Bartel" => "barteljan@yahoo.de" }
  s.source           = { :git => "https://bitbucket.org/barteljan/messagecontainerviewcontroller.git", :tag => s.version.to_s }
  s.social_media_url = 'https://twitter.com/janbartel'

  s.swift_version = '4.2'
  
  s.platform     = :ios, '9.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/**/*'

end
